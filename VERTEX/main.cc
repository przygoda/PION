#include "data.h"
#include "PPim.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>

using namespace std;
using namespace PATData;


int main()
{


/************************************** O U T P U T   F I L E ******************************************/
   outFileData = new TFile("pion800_vertex_0.root","recreate");
/*******************************************************************************************************/

/************************** control ntuple ***************************/
   ptrHNT = new TNtuple("vertex","vertex","vX:vY:vZ:seqNumber");
   //ptrHNT->setFile( outFileData );
/*********************************************************************/

   p = new TLorentzVector(0,0,0,0);
   pim = new TLorentzVector(0,0,0,0);


   PPim t;
   cout << "START!" << endl;
   t.Loop();

/***************************** F I N A L     C A L C U L A T I O N S ********************/

   outFileData->cd();
   ptrHNT->Write();

   outFileData->Close();

}

