#include "data.h"
#include <TComplex.h>

/**************************** global histograms repository ***********************************/

namespace PATData {

   TFile         *outFileData;
   TNtuple       *ptrHNT;

   TLorentzVector *p;
   TLorentzVector *pim;

   const double D2R = 1.74532925199432955e-02;
   const double R2D = 57.2957795130823229;

   // --------------------------------------------------------------------
   double openingangle(TLorentzVector a, TLorentzVector b)
   {
         return TMath::ACos( (a.Px()*b.Px() + a.Py()*b.Py() +  a.Pz()*b.Pz() ) / ( a.Vect().Mag() * b.Vect().Mag() ) );
   }

}

/*********************************************************************************************/

