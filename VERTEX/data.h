#ifndef DATA_H
#define DATA_H

#include <TROOT.h>
#include <TFile.h>
#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCutG.h>
#include <TCut.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TNtuple.h>
#include "hntuple.h"


/**************************** global histograms repository ***********************************/


namespace PATData {

   extern TFile         *outFileData;
   extern TNtuple       *ptrHNT;

   extern TLorentzVector *p;
   extern TLorentzVector *pim;

   extern const double D2R;
   extern const double R2D;

   double openingangle(TLorentzVector a, TLorentzVector b);

}

/********************************************************************************************/


#endif // DATA_H
