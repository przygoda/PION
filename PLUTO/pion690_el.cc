#include "TString.h"
#include "TFile.h"
#include "TH1F.h"
#include "TNtuple.h"
#include "PUtils.h"
#include "PTCrossWeight.h"
#include "PReaction.h"
#include "PVertexFile.h"
#include "PBeamSmearing.h"

int main(int argc, char **argv)
{
  
    PUtils::SetSeed(0);
    makeDistributionManager()->Exec("elementary");

    // Legendre polynomial coefficients A1 - A5 for 690 (Ecm = 1496 -> our 1490.26)
    /*
    double A0 = 1.52;
    double A1 = 2.23;
    double A2 = 2.42;
    double A3 = 0.41;
    double A4 = 0.07;
    double A5 = 0.20;
    */

    TF1 *angle2 = new TF1("angle2","1.52*1+2.23*(-x)+2.42*0.5*(3*(-x)*(-x)-1)+0.41*0.5*(5*(-x)*(-x)*(-x)-3*(-x))+0.07*0.125*(35*(-x)*(-x)*(-x)*(-x)-30*(-x)*(-x)+3)+0.20*0.125*(63*(-x)*(-x)*(-x)*(-x)*(-x)-70*(-x)*(-x)*(-x)+15*(-x))",-1,1);
    PAngularDistribution *ang2 = new PAngularDistribution("my_ang2","PiP2PiP from paper");
    //ang2->Add("pi-","GRANDPARENT");
    //ang2->Add("p","GRANDPARENT");
    ang2->Add("q,parent,reference");
    ang2->Add("p,daughter,primary");
    ang2->Add("pi-,daughter");
    ang2->SetAngleFunction(angle2);
    ang2->Print();
    makeDistributionManager()->Add(ang2);


    makeDistributionManager()->Print("root");
    makeDistributionManager()->Print("decay_models");
    PBeamSmearing *smear = new PBeamSmearing("beam_smear", "Beam smearing");
    smear->SetReaction("pi- + p");
    TFile hf("/lustre/nyx/hades/user/przygoda/PION/PLUTO/DATA/mom690_pt_calibrated.root");
    TH1F* mom690_pt = (TH1F*)hf.Get("mom690_pt");
    smear->SetMomentumFunction(mom690_pt); // this is exact smearing shape defined by TF1
    makeDistributionManager()->Add(smear);

//int i = 0;

TString tablica[] = {
   "pion690_vertex_0.root",
   "pion690_vertex_1.root",
   "pion690_vertex_2.root",
   "pion690_vertex_3.root",
   "pion690_vertex_4.root",
   "pion690_vertex_5.root",
   "pion690_vertex_6.root",
   "pion690_vertex_7.root",
   "pion690_vertex_8.root",
   "pion690_vertex_9.root",
   "pion690_vertex_10.root",
   "pion690_vertex_11.root",
   "pion690_vertex_12.root",
   "pion690_vertex_13.root",
   "pion690_vertex_14.root",
   "pion690_vertex_15.root",
   "pion690_vertex_16.root",
   "pion690_vertex_17.root",
   "pion690_vertex_18.root",
   "pion690_vertex_19.root",
   "pion690_vertex_20.root",
   "pion690_vertex_21.root"
};


int liczbaPlikow = sizeof(tablica)/sizeof(TString);
cout << liczbaPlikow <<endl;
for(int i = 0 ; i<liczbaPlikow; i++)
{
  TString inputFile = tablica[i];
  TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/VERTEX/FILES/";      // exp
  TString outputFile  = "/lustre/nyx/hades/user/przygoda/PION/PLUTO/FILES/EL/690/pion690_el";
  outputFile.Append(Form("_%i",i));

    
   //// --- adjusted momentum ---
   //PReaction *my_reaction =  new PReaction("0.529254825963781483","pi-","p","p pi-",(char*)outputFile.Data(),1,0,1,1); // 0.654.1 GeV
   PReaction *my_reaction =  new PReaction("0.558034537473383718","pi-","p","p pi-",(char*)outputFile.Data(),1,0,1,1); // 0.683.5 GeV
   //PReaction *my_reaction =  new PReaction("0.612396126989515732","pi-","p","p pi-",(char*)outputFile.Data(),1,0,1,1); // 0.738.9 GeV
   //PReaction *my_reaction =  new PReaction("0.66726001687283178","pi-","p","p pi-",(char*)outputFile.Data(),1,0,1,1); // 0.790.8 GeV

    TString vertex_ntuple = inputDir + inputFile;

    //Construct the vertex container:
     PVertexFile *vertex = new PVertexFile();
     vertex->OpenFile(vertex_ntuple);
           
    //add to prologue action
    my_reaction->AddPrologueBulk(vertex);
    
    my_reaction->Print();


   TFile *f = new TFile(vertex_ntuple.Data());
   if(f==NULL)
   {
       cout << "file not found" << endl;
       exit(1);

   }
   TNtuple *vertexnt = (TNtuple*)(f->Get("vertex"));
   if(vertexnt==NULL)
   {
       cout << "NULL pointer to ntuple" << endl;
       exit(1);
   }

   // number of events
   my_reaction->Loop(100000);
   f->Close();
//    my_reaction->Loop(1000000);
}
return 0;
}
