#include "TString.h"
#include "TFile.h"
#include "TH1F.h"
#include "TNtuple.h"
#include "PUtils.h"
#include "PTCrossWeight.h"
#include "PReaction.h"
#include "PVertexFile.h"
#include "PBeamSmearing.h"

int main(int argc, char **argv)
{
  
    PUtils::SetSeed(0);
    makeDistributionManager()->Exec("elementary");
    makeDistributionManager()->Print("root");
    makeDistributionManager()->Print("decay_models");
    PBeamSmearing *smear = new PBeamSmearing("beam_smear", "Beam smearing");
    smear->SetReaction("pi- + p");
    TFile hf("/lustre/nyx/hades/user/przygoda/PION/PLUTO/DATA/mom690_pt_calibrated.root");
    TH1F* mom690_pt = (TH1F*)hf.Get("mom690_pt");
    smear->SetMomentumFunction(mom690_pt); // this is exact smearing shape defined by TF1
    makeDistributionManager()->Add(smear);

int i = 0;

TString tablica[] = {
   "pion690_vertex_0.root",
   "pion690_vertex_1.root",
   "pion690_vertex_2.root",
   "pion690_vertex_3.root",
   "pion690_vertex_4.root",
   "pion690_vertex_5.root",
   "pion690_vertex_6.root",
   "pion690_vertex_7.root",
   "pion690_vertex_8.root",
   "pion690_vertex_9.root",
   "pion690_vertex_10.root",
   "pion690_vertex_11.root",
   "pion690_vertex_12.root",
   "pion690_vertex_13.root",
   "pion690_vertex_14.root",
   "pion690_vertex_15.root",
   "pion690_vertex_16.root",
   "pion690_vertex_17.root",
   "pion690_vertex_18.root",
   "pion690_vertex_19.root",
   "pion690_vertex_20.root",
   "pion690_vertex_21.root"
};


int liczbaPlikow = sizeof(tablica)/sizeof(TString);
cout << liczbaPlikow <<endl;
for(int i = 0 ; i<liczbaPlikow; i++)
{
  TString inputFile = tablica[i];
  TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/VERTEX/FILES/";      // exp
  TString outputFile  = "/lustre/nyx/hades/user/przygoda/PION/PLUTO/FILES/N/690/pion690_n_ps";
  outputFile.Append(Form("_%i",i));

    
   //// --- adjusted momentum ---
   //PReaction *my_reaction =  new PReaction("0.529254825963781483","pi-","p","n pi+ pi-",(char*)outputFile.Data(),1,0,1,1); // 0.654.1 GeV
   PReaction *my_reaction =  new PReaction("0.558034537473383718","pi-","p","n pi+ pi-",(char*)outputFile.Data(),1,0,1,1); // 0.683.5 GeV
   //PReaction *my_reaction =  new PReaction("0.612396126989515732","pi-","p","n pi+ pi-",(char*)outputFile.Data(),1,0,1,1); // 0.738.9 GeV
   //PReaction *my_reaction =  new PReaction("0.66726001687283178","pi-","p","n pi+ pi-",(char*)outputFile.Data(),1,0,1,1); // 0.790.8 GeV


    TString vertex_ntuple = inputDir + inputFile;

    //Construct the vertex container:
     PVertexFile *vertex = new PVertexFile();
     vertex->OpenFile(vertex_ntuple);
           
    //add to prologue action
    my_reaction->AddPrologueBulk(vertex);
    
    my_reaction->Print();


   TFile *f = new TFile(vertex_ntuple.Data());
   if(f==NULL)
   {
       cout << "file not found" << endl;
       exit(1);

   }
   TNtuple *vertexnt = (TNtuple*)(f->Get("vertex"));
   if(vertexnt==NULL)
   {
       cout << "NULL pointer to ntuple" << endl;
       exit(1);
   }

   // number of events
   my_reaction->Loop(100000);
   f->Close();
//    my_reaction->Loop(1000000);
}
return 0;
}
